package test;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.List;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import main.CalculateCart;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.w3c.dom.Document;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;
import org.xml.sax.SAXParseException;

public class TestCalculateCart {
	
	private CalculateCart calculateCart;
	private DocumentBuilder dBuilder;
	
	@Rule
	public final ExpectedException exception = ExpectedException.none();
	
	@Before
	public void setup() throws ParserConfigurationException {
		calculateCart = new CalculateCart();
		DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
		dBuilder = dbFactory.newDocumentBuilder();
	}
	
	//Test case for testing readXMLFile method when categories XML is called.
	@Test
	public void testReadXMLFileWhenCategoriesXMLCalled() throws ParserConfigurationException, SAXException, IOException {
		calculateCart.readXMLFile("resources/Categories.xml");
		
		Assert.assertEquals(calculateCart.getCategoryMap().keySet().toString(), "[1, 2, 3, 4, 5]");
		Assert.assertEquals(calculateCart.getCategoryMap().get(1).getName(), "ConsumerGoods");
	}
	
	//Test case for testing readXMLFile method when FlatDiscountSlabs XML is called.
	@Test
	public void testReadXMLFileWhenFlatDiscountSlabsXMLCalled() throws ParserConfigurationException, SAXException, IOException {
		calculateCart.readXMLFile("resources/FlatDiscountSlabs.xml");
		
		Assert.assertEquals(calculateCart.getSlabList().get(0).getDiscountPercent(), 2);
		Assert.assertEquals(calculateCart.getSlabList().get(0).getRangeMax(), 3000);
		Assert.assertEquals(calculateCart.getSlabList().get(0).getRangeMin(), 0);
	}
	
	//Test case for testing readXMLFile method when ShoppingCart XML is called.
	@Test
	public void testReadXMLFileWhenShoppingCartXMLCalled() throws ParserConfigurationException, SAXException, IOException {
		calculateCart.readXMLFile("resources/ShoppingCart.xml");
		
		Assert.assertEquals(calculateCart.getShoppingCartList().get(0).getItemName(), "Muesli");
		Assert.assertEquals(calculateCart.getShoppingCartList().get(0).getQuantity(), 2);
		Assert.assertEquals(calculateCart.getShoppingCartList().get(0).getUnitPrice(), 100);
		Assert.assertEquals(calculateCart.getShoppingCartList().get(0).getItemCategoryID(), 3);
		Assert.assertEquals(calculateCart.getShoppingCartList().get(0).getItemID(), 1);
	}
	
	//Test case for testing readXMLFile method when file which is not present is called.
	@Test(expected = FileNotFoundException.class)
	public void testReadXMLFileWhenWrongXMLCalled() throws ParserConfigurationException, SAXException, IOException {
		calculateCart.readXMLFile("resources/abc.xml");
	}
	
	//Test case for testing readXMLFile method when no XML is passed.
	@Test(expected = SAXParseException.class)
	public void testReadXMLFileWhenNoXMLCalled() throws ParserConfigurationException, SAXException, IOException {
		calculateCart.readXMLFile("");
	}
	
	//Test case for testing generateBill method when complete data is passed.
	@Test
	public void testGenerateBill() throws ParserConfigurationException, SAXException, IOException {
		calculateCart.readXMLFile("resources/Categories.xml");
		calculateCart.readXMLFile("resources/FlatDiscountSlabs.xml");
		calculateCart.readXMLFile("resources/ShoppingCart.xml");
		
		Assert.assertEquals("Cart Value should be same",calculateCart.generateBill(), 1601.516, 1);
	}
	
	//Test case for testing populateCategory and calculateCartNode method when categories XML is called.
	@Test
	public void testPopulateCategory() throws IOException, SAXException {
		File file = new File("resources/Categories.xml");
		Document document = dBuilder.parse(file);
		NodeList nodeList = document.getElementsByTagName("Category");
		
		calculateCart.populateCategory(nodeList);
		
		Assert.assertEquals(calculateCart.getCategoryMap().get(1).getDiscountPercent(), 5);
		Assert.assertEquals(calculateCart.getCategoryMap().get(1).getName(), "ConsumerGoods");
		
		Assert.assertEquals(calculateCart.getCategoryMap().get(2).getDiscountPercent(), 7);
		Assert.assertEquals(calculateCart.getCategoryMap().get(2).getName(), "OrganicFood");
		
		Assert.assertEquals(calculateCart.getCategoryMap().get(3).getDiscountPercent(), 2);
		Assert.assertEquals(calculateCart.getCategoryMap().get(3).getName(), "Grocery");
		
		Assert.assertEquals(calculateCart.getCategoryMap().get(4).getDiscountPercent(), 10);
		Assert.assertEquals(calculateCart.getCategoryMap().get(4).getName(), "BabyProducts");
	}
	
	//Test case for testing populateCategory and calculateCartNode method when wrong XML is called.
	@Test
	public void testPopulateCategoryWhenWrongXMLPassed() throws IOException, SAXException {
		File file = new File("resources/FlatDiscountSlabs.xml");
		Document document = dBuilder.parse(file);
		NodeList nodeList = document.getElementsByTagName("Category");
		
		calculateCart.populateCategory(nodeList);
		Assert.assertEquals(0, calculateCart.getCategoryMap().size());
	}
	
	//Test case for testing populateSlab and readSlabNode method when FlatDiscountSlabs XML is called.
	@Test
	public void testPopulateSlab() throws IOException, SAXException {
		File file = new File("resources/FlatDiscountSlabs.xml");
		Document document = dBuilder.parse(file);
		NodeList nodeList = document.getElementsByTagName("Slab");
		
		calculateCart.populateSlab(nodeList);
		
		Assert.assertEquals(calculateCart.getSlabList().get(0).getDiscountPercent(), 2);
		Assert.assertEquals(calculateCart.getSlabList().get(0).getRangeMax(), 3000);
		Assert.assertEquals(calculateCart.getSlabList().get(0).getRangeMin(), 0);
		
		Assert.assertEquals(calculateCart.getSlabList().get(1).getDiscountPercent(), 4);
		Assert.assertEquals(calculateCart.getSlabList().get(1).getRangeMax(), 7000);
		Assert.assertEquals(calculateCart.getSlabList().get(1).getRangeMin(), 3001);
		
		Assert.assertEquals(calculateCart.getSlabList().get(2).getDiscountPercent(), 5);
		Assert.assertEquals(calculateCart.getSlabList().get(2).getRangeMax(), 8000);
		Assert.assertEquals(calculateCart.getSlabList().get(2).getRangeMin(), 7000);
	}
	
	//Test case for testing populateSlab and readSlabNode method when wrong XML is called.
	@Test
	public void testPopulateSlabWhenWrongXML() throws IOException, SAXException {
		File file = new File("resources/Categories.xml");
		Document document = dBuilder.parse(file);
		NodeList nodeList = document.getElementsByTagName("Slab");
		
		calculateCart.populateSlab(nodeList);
		Assert.assertEquals(0, calculateCart.getSlabList().size());
	}

	//Test case for testing fetchShoppingCart method when wrong XML is called.
	@Test
	public void testFetchShoppingCartWhenWrongXML() throws IOException, SAXException {
		File file = new File("resources/Categories.xml");
		Document document = dBuilder.parse(file);
		
		List<NodeList> shoppingCartNodeList = calculateCart.fetchShoppingCart(document);
		Assert.assertEquals(0, shoppingCartNodeList.get(0).getLength());
	}
	
	//Test case for testing fetchShoppingCart method when ShoppingCart XML is called.
	@Test
	public void testFetchShoppingCart() throws IOException, SAXException {
		File file = new File("resources/ShoppingCart.xml");
		Document document = dBuilder.parse(file);
		
		List<NodeList> shoppingCartNodeList = calculateCart.fetchShoppingCart(document);
		
		Assert.assertEquals("000001", shoppingCartNodeList.get(0).item(0).getTextContent());
		Assert.assertEquals("003", shoppingCartNodeList.get(1).item(0).getTextContent());
		Assert.assertEquals("Muesli", shoppingCartNodeList.get(2).item(0).getTextContent());
		Assert.assertEquals("100", shoppingCartNodeList.get(3).item(0).getTextContent());
		Assert.assertEquals("2", shoppingCartNodeList.get(4).item(0).getTextContent());
	}
	
	//Test case for testing populateShoppingCart method when ShoppingCart XML is called.
	@Test
	public void testPopulateShoppingCart() throws IOException, SAXException {
		File file = new File("resources/ShoppingCart.xml");
		Document document = dBuilder.parse(file);
		
		List<NodeList> shoppingCartNodeList = calculateCart.fetchShoppingCart(document);
		calculateCart.populateShoppingCart(shoppingCartNodeList);
		
		Assert.assertEquals(1, calculateCart.getShoppingCartList().get(0).getItemID());
		Assert.assertEquals(3, calculateCart.getShoppingCartList().get(0).getItemCategoryID());
		Assert.assertEquals("Muesli", calculateCart.getShoppingCartList().get(0).getItemName());
		Assert.assertEquals(2, calculateCart.getShoppingCartList().get(0).getQuantity());
		Assert.assertEquals(100, calculateCart.getShoppingCartList().get(0).getUnitPrice());
	}
}
