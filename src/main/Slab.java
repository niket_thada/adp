package main;

public class Slab {
	int rangeMin;
	int rangeMax;
	int discountPercent;
	
	public Slab() {
		super();
	}

	public Slab(int rangeMin, int rangeMax, int discountPercent) {
		super();
		this.rangeMin = rangeMin;
		this.rangeMax = rangeMax;
		this.discountPercent = discountPercent;
	}

	public int getRangeMin() {
		return rangeMin;
	}

	public void setRangeMin(int rangeMin) {
		this.rangeMin = rangeMin;
	}

	public int getRangeMax() {
		return rangeMax;
	}

	public void setRangeMax(int rangeMax) {
		this.rangeMax = rangeMax;
	}

	public int getDiscountPercent() {
		return discountPercent;
	}

	public void setDiscountPercent(int discountPercent) {
		this.discountPercent = discountPercent;
	}

	@Override
	public String toString() {
		return "Slab [rangeMin=" + rangeMin + ", rangeMax=" + rangeMax
				+ ", discountPercent=" + discountPercent + "]";
	}
}
