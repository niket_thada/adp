package main;
import java.io.File;
import java.io.IOException;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

public class CalculateCart {
	private Map<Integer,Category> categoryMap = new HashMap<Integer, Category>();
	private List<ShoppingCart> shoppingCartList = new ArrayList<ShoppingCart>();
	private List<Slab> slabList = new ArrayList<Slab>();
	//Declaring static variables.
	private static final String CATEGORIES_NODE_NAME = "Categories";
	private static final String CATEGORY_NODE_NAME = "Category";
	private static final String FLAT_DISCOUNT_SLABS_NODE_NAME = "FlatDiscountSlabs";
	private static final String SLAB_NODE_NAME = "Slab";
	private static final String SHOPPING_CART_NODE_NAME = "ShoppingCart";
	private static final String ITEM_ID_NODE_NAME = "itemID";
	private static final String ITEM_CATEGORY_ID_NODE_NAME = "itemCategoryID";
	private static final String ITEM_NAME_NODE_NAME = "itemName";
	private static final String UNIT_PRICE_NODE_NAME = "unitPrice";
	private static final String QUANTITY_NODE_NAME = "quantity";
	private static final String ID_TAG_NAME = "id";
	private static final String NAME_TAG_NAME = "name";
	private static final String DISCOUNT_PERCENT_TAG_NAME = "discPerc";
	private static final String RANGE_MIN_TAG_NAME = "RangeMin";
	private static final String RANGE_MAX_TAG_NAME = "RangeMax";
	private static final String CATEGORIES_XML = "resources/Categories.xml";
	private static final String FLAT_DISCOUNT_SLABS_XML = "resources/FlatDiscountSlabs.xml";
	private static final String SHOPPING_CART_XML = "resources/ShoppingCart.xml";
	
	//Main method for execution
	public static void main(String args[]) {
		CalculateCart calculateCart = new CalculateCart();
		try {
			calculateCart.readXMLFile(CATEGORIES_XML);
			calculateCart.readXMLFile(FLAT_DISCOUNT_SLABS_XML);
			calculateCart.readXMLFile(SHOPPING_CART_XML);
			calculateCart.generateBill();
		} catch (IOException | SAXException | ParserConfigurationException ex) {
			ex.printStackTrace();
		}
	}
	
	//Method to read XML resources provided.
	public void readXMLFile (String filePath) throws ParserConfigurationException, SAXException, IOException {
		File file = new File(filePath);
		DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
		DocumentBuilder dBuilder;
		dBuilder = dbFactory.newDocumentBuilder();
		Document document = dBuilder.parse(file);
		document.getDocumentElement().normalize();
		NodeList nodeList = null;
        if (CATEGORIES_NODE_NAME.equals(document.getDocumentElement().getNodeName())) {
        	// Fetching nodelist from categories xml by tag name and populating categories map
        	nodeList = document.getElementsByTagName(CATEGORY_NODE_NAME);
        	populateCategory(nodeList);
        } else if (FLAT_DISCOUNT_SLABS_NODE_NAME.equals(document.getDocumentElement().getNodeName())) {
        	// Fetching nodelist from slab xml by tag name and populating slab list
        	nodeList = document.getElementsByTagName(SLAB_NODE_NAME);
        	populateSlab(nodeList);
        } else if (SHOPPING_CART_NODE_NAME.equals(document.getDocumentElement().getNodeName())) {
        	// Method call for populating shopping cart list
        	populateShoppingCart(fetchShoppingCart(document));
        }
	}
	
	//Method for reading shopping cart XML nodes and returning it's listin order to populate shopping cart 
	public List<NodeList> fetchShoppingCart(Document document) {
		List<NodeList> shoppingCartXMLNodeList = new ArrayList<NodeList>();
		
		NodeList itemNodeList = document.getElementsByTagName(ITEM_ID_NODE_NAME);
    	NodeList itemCategoryNodeList = document.getElementsByTagName(ITEM_CATEGORY_ID_NODE_NAME);
    	NodeList itemNameNodeList = document.getElementsByTagName(ITEM_NAME_NODE_NAME);
    	NodeList unitPriceNodeList = document.getElementsByTagName(UNIT_PRICE_NODE_NAME);
    	NodeList quantityNodeList = document.getElementsByTagName(QUANTITY_NODE_NAME);
    	
    	shoppingCartXMLNodeList.add(itemNodeList);
    	shoppingCartXMLNodeList.add(itemCategoryNodeList);
    	shoppingCartXMLNodeList.add(itemNameNodeList);
    	shoppingCartXMLNodeList.add(unitPriceNodeList);
    	shoppingCartXMLNodeList.add(quantityNodeList);
    	
    	return shoppingCartXMLNodeList;
	}
	
	//Method for reading categories XML by node list and populating category map.
	public void populateCategory (NodeList nodeList) {
		// Looping over node list for category. 
        for (int i=0; i < nodeList.getLength(); i++ ) {
        	Category category = readCategoryNode(nodeList.item(i));
        	categoryMap.put(category.getId(), category);
        }
	}
	
	//Method for reading slab XML by node list and populating slab list.
	public void populateSlab (NodeList nodeList) {
		// Looping over node list for slab.
        for (int i=0; i < nodeList.getLength(); i++ ) {
        	slabList.add(readSlabNode(nodeList.item(i)));
        }
	}
	
	//Method for populating shopping cart list with the help of list of node list provided from shopping cart XML.
	public void populateShoppingCart (List<NodeList> shoppingCartNodeList) {
		NodeList itemNodeList = shoppingCartNodeList.get(0);
    	NodeList itemCategoryNodeList = shoppingCartNodeList.get(1);
    	NodeList itemNameNodeList = shoppingCartNodeList.get(2);
    	NodeList unitPriceNodeList = shoppingCartNodeList.get(3);
    	NodeList quantityNodeList = shoppingCartNodeList.get(4);
		
		int cartTotalItems = itemNodeList.getLength();
		
		try {
			// looping total cart item in shopping cart XML.
			for (int currentItem=0; currentItem < cartTotalItems; currentItem++) {
				Node itemNode = (Node) itemNodeList.item(currentItem);
				Node itemCategoryNode = (Node) itemCategoryNodeList.item(currentItem);
				Node itemNameNode = (Node) itemNameNodeList.item(currentItem);
				Node unitPriceNode = (Node) unitPriceNodeList.item(currentItem);
				Node quantityNode = (Node) quantityNodeList.item(currentItem);
				
				// Condition to check whether any of the node is not present in shopping cart XML.
				if (itemNode != null && itemCategoryNode != null && itemNameNode != null && 
						unitPriceNode != null && quantityNode != null) {
					ShoppingCart shoppingcart = new ShoppingCart(Integer.parseInt(itemNode.getTextContent()), 
							Integer.parseInt(itemCategoryNode.getTextContent()), itemNameNode.getTextContent(),
							Integer.parseInt(unitPriceNode.getTextContent()),Integer.parseInt(quantityNode.getTextContent()));
					
					shoppingCartList.add(shoppingcart);
				} else {
					System.out.println("Shopping cart XML provided does not contain complete data");
					break;
				}
			}
		} catch (NumberFormatException ex) {
			ex.printStackTrace();
		}
	}
	
	//Reading id, name and discount percent from category node provided.
	private static Category readCategoryNode(Node node) {
		Category category = new Category();
		try {
	        if (node.getNodeType() == Node.ELEMENT_NODE) {
	            Element element = (Element) node;
	            category.setId(Integer.parseInt(getTagValue(ID_TAG_NAME, element)));
	            category.setName(getTagValue(NAME_TAG_NAME, element));
	            category.setDiscountPercent(Integer.parseInt(getTagValue(DISCOUNT_PERCENT_TAG_NAME, element)));
	        }
		} catch (NumberFormatException ex) {
			ex.printStackTrace();
		}
        return category;
	}
	
	//Reading min range, max range, discount percent from slab node provided.
	private static Slab readSlabNode(Node node) {
		Slab slab = new Slab();
		try {
	        if (node.getNodeType() == Node.ELEMENT_NODE) {
	            Element element = (Element) node;
	            slab.setRangeMin(Integer.parseInt(getTagValue(RANGE_MIN_TAG_NAME, element)));
	            slab.setRangeMax(Integer.parseInt(getTagValue(RANGE_MAX_TAG_NAME, element)));
	            slab.setDiscountPercent(Integer.parseInt(getTagValue(DISCOUNT_PERCENT_TAG_NAME, element)));
	        }
		} catch (NumberFormatException ex) {
			ex.printStackTrace();;
		}
        return slab;
	}
	
	//Method for getting tag values. 
	private static String getTagValue(String tag, Element element) {
		NodeList nodeList = element.getElementsByTagName(tag).item(0).getChildNodes();
        Node node = (Node) nodeList.item(0);
    	String nodeValue = node != null ? node.getNodeValue() : "";
        return nodeValue;
    }
	
	//Method for generating bill.
	public float generateBill() {
		DecimalFormat decimalFormat = new DecimalFormat("###.##");
		float totalCartValue = 0;
		float totalDiscount = 0;

		System.out.println("====================================================================================");
		
		//Looping over shopping cart items and applying category based discount.
		for (int currentItem = 0; currentItem < shoppingCartList.size(); currentItem++) {
			ShoppingCart shoppingItem = shoppingCartList.get(currentItem);
			Category itemCategory = categoryMap.get(shoppingCartList.get(currentItem).getItemCategoryID());
			float categorydiscount = 0;
			// check if the category of the item in shopping cart is not present in category XML.
			if (itemCategory != null) {
				categorydiscount = (shoppingItem.getUnitPrice() * shoppingItem.getQuantity() * 
						((float) itemCategory.getDiscountPercent()) / 100);
			}

			float itemTotal = (shoppingItem.getUnitPrice() * shoppingItem.getQuantity()) - categorydiscount;
			totalCartValue = totalCartValue + itemTotal;
			System.out.println("Item no. : "+(currentItem + 1));
			System.out.println("Item Name : "+shoppingItem.getItemName());
			System.out.println("Quantity : "+shoppingItem.getQuantity());
			System.out.println("UnitPrice : "+shoppingItem.getUnitPrice());
			System.out.println("Discount : "+categorydiscount);
			System.out.println("Net Purchase amount : "+ decimalFormat.format(itemTotal));
			System.out.println("--------------------------------");

			totalDiscount = totalDiscount + categorydiscount;
		}
		
		//Applying slab based discount and generating final bill.
		for (int j = 0; j < slabList.size(); j++) {
			Slab slab = slabList.get(j);
			// Check if total cart value is between min and max range.
			if (slab.getRangeMin() < totalCartValue	&& slab.getRangeMax() > totalCartValue) {
				float slabDiscount = totalCartValue	* ((float) slab.getDiscountPercent() / 100);
				totalCartValue = totalCartValue	- slabDiscount;
				totalDiscount = totalDiscount + slabDiscount;
				break;
			}
		}
		System.out.println("Grand Total:" + decimalFormat.format((totalCartValue + totalDiscount)));
		System.out.println("Applicable Discount : "+ decimalFormat.format(totalDiscount));
		System.out.println("Net Bill Amount : "+ decimalFormat.format(totalCartValue));
		return totalCartValue;
	}
	
	public Map<Integer, Category> getCategoryMap() {
		return categoryMap;
	}

	public List<ShoppingCart> getShoppingCartList() {
		return shoppingCartList;
	}

	public List<Slab> getSlabList() {
		return slabList;
	}
}
