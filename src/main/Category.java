package main;

public class Category {
	int id;
	String name;
	int discountPercent;
	
	public Category() {
		super();
	}

	public Category(int id, String name, int discountPercent) {
		super();
		this.id = id;
		this.name = name;
		this.discountPercent = discountPercent;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getDiscountPercent() {
		return discountPercent;
	}

	public void setDiscountPercent(int discountPercent) {
		this.discountPercent = discountPercent;
	}

	@Override
	public String toString() {
		return "Category [id=" + id + ", name=" + name + ", discountPercent="
				+ discountPercent + "]";
	}
}
