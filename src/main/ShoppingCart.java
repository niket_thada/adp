package main;

public class ShoppingCart {
	int itemID;
	int itemCategoryID;
	String itemName;
	int unitPrice;
	int quantity;
	
	public ShoppingCart() {
		super();
	}

	public ShoppingCart(int itemID, int itemCategoryID, String itemName,
			int unitPrice, int quantity) {
		super();
		this.itemID = itemID;
		this.itemCategoryID = itemCategoryID;
		this.itemName = itemName;
		this.unitPrice = unitPrice;
		this.quantity = quantity;
	}

	public int getItemID() {
		return itemID;
	}

	public void setItemID(int itemID) {
		this.itemID = itemID;
	}

	public int getItemCategoryID() {
		return itemCategoryID;
	}

	public void setItemCategoryID(int itemCategoryID) {
		this.itemCategoryID = itemCategoryID;
	}

	public String getItemName() {
		return itemName;
	}

	public void setItemName(String itemName) {
		this.itemName = itemName;
	}

	public int getUnitPrice() {
		return unitPrice;
	}

	public void setUnitPrice(int unitPrice) {
		this.unitPrice = unitPrice;
	}

	public int getQuantity() {
		return quantity;
	}

	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}

	@Override
	public String toString() {
		return "ShoppingCart [itemID=" + itemID + ", itemCategoryID="
				+ itemCategoryID + ", itemName=" + itemName + ", unitPrice="
				+ unitPrice + ", quantity=" + quantity + "]";
	}
}
